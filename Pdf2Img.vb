Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Collections.Generic
Imports System.Text
' required Ghostscript.NET namespaces
Imports Ghostscript.NET
Imports Ghostscript.NET.Rasterizer

Public Class ConvertPdf2Jpg
	
	Private _ghostScriptVersion As GhostscriptVersionInfo = Nothing

	Public Sub Start(ghostScriptLibPath As String, inputPdfPath As String, outputFileName As String, Page As Integer)
		Dim target_x_dpi As Integer = 300
		Dim target_y_dpi As Integer = 300

		_ghostScriptVersion = New GhostscriptVersionInfo(New Version(0, 0, 0), ghostScriptLibPath, String.Empty, GhostscriptLicense.GPL)
		Dim rasterizer As New GhostscriptRasterizer()
		
		Dim width As Integer= 0
		Dim height As Integer= 0
		
		rasterizer.Open(inputPdfPath, _ghostScriptVersion, False)

		Dim bmpList As New List(Of Bitmap)()

		' Get an image for each page in the pdf
		'For pageNumber As Integer = 1 To rasterizer.PageCount
		'Dim img As Image = rasterizer.GetPage(target_x_dpi, target_y_dpi, pageNumber)
		Dim img As Image = rasterizer.GetPage(target_x_dpi, target_y_dpi, Page)
			If img.Width > width Then
				width = img.Width
			End If
			height += img.Height


			Dim bmp As New Bitmap(img)
			Dim bmp2 As Bitmap = DirectCast(bmp.Clone(), Bitmap)
			bmpList.Add(bmp2)
		'Next


		' Add each image to the output bitmap, using canvas as the drawing vehicle 
		Dim bitmap As Bitmap = New Bitmap(width, height)
		Dim canvas As Graphics= Graphics.FromImage(bitmap)
		Dim curHeight As Integer = 0
		For i As Integer = 0 To bmpList.Count - 1
			canvas.DrawImage(bmpList(i), New Point(0, curHeight))
			curHeight += bmpList(i).Height
		Next
		canvas.Save()

		' Save the output bitmap to file.
		bitmap.Save(outputFileName, ImageFormat.Jpeg)
	End Sub
End Class


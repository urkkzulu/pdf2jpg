﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Reflection
Imports System.IO

Class Program
	
	Private Shared _ghostScriptLibPath As String = "unknown"
	Private Shared _pdfFullFilename As String = "unknown"
	Private Shared _outputImageFileName As String = "unknown"
	
	Friend Shared Sub Main(args As String())
		
		Dim ass As Assembly = Assembly.GetExecutingAssembly()
		_ghostScriptLibPath = Path.GetDirectoryName(ass.Location)
        _ghostScriptLibPath = _ghostScriptLibPath & "\gsdll64.dll"
        Dim Page As Integer
		'If args.Length <> 1 Then
			'TODO: пакетная обработка
		'End If
		
		'If args.Length = 1 Then
		'	_pdfFullFilename = args(0)
		'	_outputImageFileName = Path.GetDirectoryName(_pdfFullFilename)& "\" & System.IO.Path.GetFileNameWithoutExtension(_pdfFullFilename) & ".jpg"
		'End If
		Dim rasterizer As New ConvertPdf2Jpg()
		
		'For i As Integer = 0 To args.Length - 1 
		'	Console.WriteLine(args(i))
		_pdfFullFilename = args(0)
		_outputImageFileName = args(1)
		Page = Integer.Parse(args(2))
		
		
		'	_outputImageFileName = Path.GetDirectoryName(_pdfFullFilename)& "\" & System.IO.Path.GetFileNameWithoutExtension(_pdfFullFilename) & ".jpg"
			
			
		'	Console.WriteLine("GhostScriptlibPath=" & _ghostScriptLibPath)
		'	Console.WriteLine("PdfFullFn=" & _pdfFullFilename)
		'	Console.WriteLine("OutputImageFileName=" & _outputImageFileName)
			

		rasterizer.Start(_ghostScriptLibPath, _pdfFullFilename, _outputImageFileName, Page)
		'Next
		
		'If System.Diagnostics.Debugger.IsAttached Then
		'	Console.WriteLine()
		'	Console.WriteLine("Press any key to quit")
		'	' You are debugging 
		'	Console.ReadKey()
		'End If
		
		Environment.Exit(0)
	End Sub
	
	'TODO: add file system monitor hanler		
	'		#Region "Folder Monitoring"
	'		''' <summary>A new PDF was created in the directory transform it!</summary>
	'		''' <param name="sender"></param><param name="e"></param>
	'		Private Sub NewPDFCreated(sender As Object, e As System.IO.FileSystemEventArgs)
	'			Dim input As New System.IO.FileInfo(e.FullPath)
	'			If Not input.Exists Then
	'				MessageBox.Show("The file ""{0}"" can't be founded", txtSingleFile.Text)
	'				Return
	'			End If
	'			'You should do this on a separate thread
	'			'to be sure that the program don't freeze while converting
	'			'just for the sake of demontration i'm doing it here in this thread
	'			ConvertSingleImage(input.FullName)
	'			lblInfo.Text = String.Format("{0}:File converted! Continue to Monitor!", DateTime.Now.ToShortTimeString())
	'		End Sub
	'		#End Region
	
End Class
